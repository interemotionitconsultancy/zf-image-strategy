# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 0.3 - 2017-01-09

### Added

- Nothing

### Changes

- Dependency to zend-mvc

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.

## 0.2 - 2017-01-09

### Added

- Nothing

### Changes

- Dependency to zend-mvc

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.

## 0.1.1 - 2017-01-09

### Added

- Licence
- Changelog

### Changes

- Nothing

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing.

## 0.1 - 2017-01-09
First release